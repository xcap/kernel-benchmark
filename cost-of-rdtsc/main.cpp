#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}
inline volatile void NOP() {

	//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
	asm volatile ("nop;");
	return; 
}

inline volatile long long RDTSC1() {
   register long long TSC asm("eax");
   //asm volatile ("rdtsc" : : : "eax", "edx");
   //asm volatile (".byte 15, 49" : : : "eax", "edx");
   return TSC;
} 

#define rdtscl(low) \
	__asm__ __volatile__("rdtsc" : "=a" (low) : : "edx")

#define rdtsclx(low) \
	        __asm__ __volatile__("rdtsc" : : : )

#define DEPTH 1000

static unsigned long int calldata [DEPTH + 1];

int main(void) {

	long long t1, t0;
	long tl0, tl1, tl2;
	
	float f1 = 130.50; 
	float f2 = 23.5;
	float f3;


	for (int i = 0; i < 1000; i ++) 
	{	

	//	t0 = RDTSC();
		rdtscl(tl0);
		
	//	unsigned long long int t0;
	//	__asm__ __volatile__ ("rdtsc" : "=A" (t0));

		//f3 = f2 - f1;
		//NOP();
		//asm volatile ("nop;");
		//
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
		//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");

		//rdtsclx(tl2);
	
		rdtscl(tl1);
		
		//t1 = RDTSC();
	//	unsigned long long int t1;
        //        __asm__ __volatile__ ("rdtsc" : "=A" (t1));
		
		calldata[i] = tl1 - tl0;

		//std::cout << t1 - t0 << "\n";			
		//std::cout << tl1 - tl0 << "\n";
		//std::cout << tl2 - tl0 << "\n\n";
	}

	
        /*
         * Dump the data to console
         */
        for ( int i = 0; i < DEPTH; i ++ ) {
                std::cout << calldata[i] << "\n";
        };


	return 0;
};
