#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sys/shm.h>

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}
inline volatile void NOP() {

        //asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
        asm volatile ("nop;");
        return;
}


inline volatile unsigned long int RDTSCL() {
     unsigned long int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}

inline volatile long long RDTSC1() {
   register long long TSC asm("eax");
   //asm volatile ("rdtsc" : : : "eax", "edx");
   //asm volatile (".byte 15, 49" : : : "eax", "edx");
   return TSC;
}

#define DEPTH 1000


int shmid;
void *s_area = NULL;


#define S_SIZE 100*4096

int initialize_shm() {

	key_t key = 1116;

	shmid = shmget(key, S_SIZE, 0666);

	if (-1 == shmid) {


           if (ENOENT != errno) {
               return -2;
           }

           shmid = shmget(key, S_SIZE, IPC_CREAT | 0666);

           if (-1 == shmid) {

               return -3;

           }


       }

	s_area = shmat(shmid, NULL, 0);
	if (-1 == (long)s_area) {
		
		return -4;
	}

	return 0;	

};

#ifdef DEFINE_AMD_64
/* 64 bits for AMD 64 */
typedef unsigned long int test_t;
#endif

#ifdef DEFINE_AMD_32
/* 32 bits for AMD 64 */
typedef unsigned int test_t;
#endif

test_t (*calldata) [DEPTH + 1];
test_t (*returndata) [DEPTH + 1];

static test_t volatile local = 0;
int  * volatile signal;
test_t * volatile var; 
test_t tmp;

int main(void) {

        unsigned long int tl0, tl1;
        int ret;

	std::cerr << "Main entered\n";

	calldata = (test_t (*)[DEPTH + 1])malloc(sizeof(test_t) * (DEPTH + 1));
	returndata = (test_t (*)[DEPTH + 1])malloc(sizeof(test_t) * (DEPTH + 1));

        for ( int i = 0; i < DEPTH; i++) {
                (*calldata)[i] = 0;
                (*returndata)[i] = 0;
        };

	std::cerr << "Allocating shared memory region\n";

	ret = initialize_shm();
	if (ret != 0) {
		std::cerr << "Unable to allocate shared memory region" << ret <<"\n";
		return -1;
	};

	std::cerr << "Strart experiment: shift:" << sizeof(test_t) <<"\n";

	nice(-20);

	var = ((test_t *)s_area);
	signal = (int *)((char *)(s_area) + S_SIZE - sizeof(int));	

	
//#define SHIFT 32	
	for ( int i = 0; i < DEPTH; i ++ ) {
		
		tl0 = RDTSCL();
	
		//asm volatile ("mov %%eax, ");
		
#define SHIFT (sizeof(test_t))

#ifdef DEFINE_MEASURE_COST
#define OP(x)   __asm__ __volatile__ ("movl (%0), %%eax\n" : :"r"(((char*)var) + (x)*SHIFT): "eax")
#endif

#ifdef DEFINE_MEASURE_OVHD		
#define OP(x)   __asm__ __volatile__ ("movl (%0), %%eax\n" : :"r"(((char*)var) + (8)*SHIFT): "eax")
#endif		
		
//#define OP(x) 	asm volatile ("movl (%0), %%eax\n" : :"r"(var + (8)*SHIFT): "eax")
//#define OP(x) 		asm volatile ("movl (%0), %%eax\n" :"=a"(tmp) :"r"( (var + (x)*SHIFT)) ); \
				local +=tmp;
//#define OP(x) 		asm volatile ("movl %0, %%eax\n" :"=a"(tmp) :"r"( *(long long int *)((char*)var + (x)*SHIFT*sizeof(long long int))) ); \
			local +=tmp;

		
//#define OP10(_x) OP(0x ## _x ##0); OP(0x ## _x ##1); OP(0x ## _x ##2); OP(0x ## _x ##3); \
//		 OP(0x ## _x ##4); OP(0x ## _x ##5); OP(0x ## _x ##6); OP(0x ## _x ##7); \
//		 OP(0x ## _x ##8); OP(0x ## _x ##1); 

#define OP10(_x) OP( (_x *10 + 0) ); OP( (_x *10 + 1) ); OP( (_x *10 + 2) ); OP( (_x *10 + 3) ); \
		 OP( (_x *10 + 4) ); OP( (_x *10 + 5) ); OP( (_x *10 + 6) ); OP( (_x *10 + 7) ); \
		 OP( (_x *10 + 8) ); OP( (_x *10 + 9) ); 

#define OP100(_x) OP10( (_x *10 + 0) ); OP10( (_x *10 + 1) ); OP10( (_x *10 + 2) ); OP10( (_x *10 + 3) ); \
		  OP10( (_x *10 + 4) ); OP10( (_x *10 + 5) ); OP10( (_x *10 + 6) ); OP10( (_x *10 + 7) ); \
		  OP10( (_x *10 + 8) ); OP10( (_x *10 + 9) ); 

#define OP1000(_x) OP100( (_x *10 + 0) ); OP100( (_x *10 + 1) ); OP100( (_x *10 + 2) ); OP100( (_x *10 + 3) ); \
		   OP100( (_x *10 + 4) ); OP100( (_x *10 + 5) ); OP100( (_x *10 + 6) ); OP100( (_x *10 + 7) ); \
		   OP100( (_x *10 + 8) ); OP100( (_x *10 + 9) ); 

/*
 * Pseudo random order of memory access 
 */
#define PR_OP10(_x) OP( (_x *10 + 8) ); OP( (_x *10 + 4) ); OP( (_x *10 + 9) ); OP( (_x *10 + 5) ); \
		    OP( (_x *10 + 2) ); OP( (_x *10 + 6) ); OP( (_x *10 + 3) ); OP( (_x *10 + 0) ); \
		    OP( (_x *10 + 7) ); OP( (_x *10 + 1) ); 

#define PR_OP100(_x) PR_OP10( (_x *10 + 5) ); PR_OP10( (_x *10 + 4) ); PR_OP10( (_x *10 + 8) ); PR_OP10( (_x *10 + 2) ); \
		     PR_OP10( (_x *10 + 0) ); PR_OP10( (_x *10 + 3) ); PR_OP10( (_x *10 + 9) ); PR_OP10( (_x *10 + 6) ); \
		     PR_OP10( (_x *10 + 1) ); PR_OP10( (_x *10 + 7) ); 

#define PR_OP1000(_x) PR_OP100( (_x *10 + 1) ); PR_OP100( (_x *10 + 6) ); PR_OP100( (_x *10 + 3) ); PR_OP100( (_x *10 + 4) ); \
		      PR_OP100( (_x *10 + 2) ); PR_OP100( (_x *10 + 7) ); PR_OP100( (_x *10 + 8) ); PR_OP100( (_x *10 + 5) ); \
		      PR_OP100( (_x *10 + 9) ); PR_OP100( (_x *10 + 0) ); 
		
		/*
#define OP100(_x) OP10( (_x *10 + 0) ); OP10( _x ##1); OP10( _x ##2); OP10( _x ##3); \
		  OP10( (_x *10 + 4) ); OP10( _x ##5); OP10( _x ##6); OP10( _x ##7); \
		  OP10( (_x * 10 + 8)); OP10( (_x *10 + 9 ) ); 		

#define OP1000(_x) OP100( _x ##0); OP100( _x ##1); OP100( _x ##2); OP100( _x ##3); \
 		   OP100( _x ##4); OP100( _x ##5); OP100( _x ##6); OP100( _x ##7); \
		   OP100( _x ##8); OP100( _x ##9); 		
*/

#ifdef DEFINE_SNOOP_4000
#define SNOOP_DEPTH  4096
#endif

#ifdef DEFINE_SNOOP_2000			
#define SNOOP_DEPTH 2048
#endif 			

#if DEFINE_ACCESS_IN_A_LOOP
		/*
		 * Opteron
		 * -------
		 *
		 * Opteron has 64KBL1 data cache 
		 * 2-way associative. Cache line is 64 bytes (stores 8 = 4 + 4  64-bit (8 bytes) values, 1024 lines) 
		 *
		 *  
		 * -O2 (with add operation)
		 *   Snoop one variable overhead: same CPU 52611 - 57531
		 *   			 	different CPUs 57531
		 *   Snoop cache 
		 *              57522 different CPUs
		 *              57558 same CPU
		 *   
		 * -O2 (no add operation) 
		 *   Snoop variable (loop overhead)
		 *              33049 same CPU
		 *              32931 diff CPUs
		 *   Snoop cache 
		 *              62292 same CPU
		 *              67705 different CPUs
		 */

		
		for (int j = 0; j < SNOOP_DEPTH; j++) {
			OP(j);
		};
#endif
		
#if 1		
		/*
		 * (with add operation) 
		 * -O1
		 * Snoop one variable overhead: 105930 -138891 (Any CPU)
		 *   Snoop cache 130451 - 188785 (diff CPUs)
		 *               130036 same CPU
		 *  
		 * -O2
		 *   Snoop one variable overhead: 105770 - 
		 *   Snoop cache 
		 *              127136 different CPUs
		 *              127098 same CPU
		 *   
		 * (no add operation) 
		 * -O2
		 *   Snoop one variable overhead: 34143
		 *   Snoop cache 
		 *               69950 different CPUs
		 *               68836 same CPU
  		 *
		 */

#if 0	/* opteron 8192 operations (1024 lines each storing 8 values) */
		
		OP1000(0);
		OP1000(1);
		OP1000(2);
		OP1000(3);
		OP1000(4);
		OP1000(5);
		OP1000(6);
		OP1000(7);
		OP100(80);
		OP100(81);

#endif 
		/*
		 * Xeon
		 * ----
		 * 
                 * Xeon has the following cache configurations:
                 * L1: 64 bytes line size; it can be either 8KB 4-way (128 lines, 32 sets) or 16KB 8-way (256 lines, 32 sets)
                 * L2: (this machine has 1024KB), 8-way, 64 bytes line size
                 * L3: can be 512KB, 1MB, 2MB, or 4MB, 8-way, 64 bytes line size
		 *  
		 * 
		 *
		 *
		 */

#if DEFINE_SEQ_4000    /* Xeon 4096 operations (assumming 16KB cache, it has 256 lines each storing 16 4byte values) */
		/* Sequential access */

  		OP1000(0);
		OP1000(1);
		OP1000(2);
		OP1000(3);

		OP10(400);
		OP10(401);
		OP10(402);
		OP10(403);
		OP10(404);
		OP10(405);
		OP10(406);
		OP10(407);
		OP10(408);
		
		OP(4090);
		OP(4091);
		OP(4092);
		OP(4093);
		OP(4094);
		OP(4095);		

#endif

#if DEFINE_SEQ_2000 /* Xeon 2048 operations (assumming 8KB cache, it has 128 lines each storing 16 4byte values) */
		/* Sequential access */

  		OP1000(0);
		OP1000(1);
		
		OP10(200);
		OP10(201);
		OP10(202);
		OP10(203);
		
		OP(2040);
		OP(2041);
		OP(2042);
		OP(2043);
		OP(2044);
		OP(2045);		
		OP(2046);
		OP(2047);		

#endif
		
#endif
		
#if 0   /* Opteron */		
		/*
		 * Pseudo random memory referencing
		 *
		 * (with add operation) 
		 *   
		 * -O2
		 *   Snoop one variable overhead: 106149
		 *   Snoop cache 
		 *                158764 different CPUs
		 *                177128 same CPU (???? strange number)
		 *   
		 * (no add operation) 
		 * -O2
		 *   Snoop one variable overhead: 32950 ~same
		 *   Snoop cache 
		 *                81530 - 83853 different CPUs
		 *                80447 - same CPU
  		 *
		 */

  		PR_OP1000(0);
		PR_OP1000(1);
		PR_OP1000(2);
		PR_OP1000(3);
		PR_OP1000(4);
		PR_OP1000(5);
		PR_OP1000(6);
		PR_OP1000(7);
		PR_OP100(80);
		PR_OP100(81);

#endif
		
#if DEFINE_PSEUDO_4000 
		
		/* Xeon 4096 operations (assumming 16KB cache, it has 256 
		   lines each storing 16 4byte values) */
		/* Pseudorandom access */

		/*
		 * Snoop 16KB cache (4096 cache elements), SMP cores
		 *           Pseudorandom access: 35745 
		 *           Sequential access: 24308, loop overhead: 18007
		 *           Sequential access in a loop: 26092, loop overhead: 22283
		 *  
		 *  Hyperthreaded core:
		 *           Pseudorandom access: 21817, loop overhead 18270 
		 *           Sequential access: 23025, 
		 *           Sequential access in a loop: , loop overhead: 
		 *
		 * Snoop 8KB cache (2048 cache elements) 
		 *
		 *   SMP cores
		 *           Pseudorandom access: 12645 - 13710
		 *           Sequential access:  8152 - 11385, loop overhead: 6630 
		 *           Sequential access in a loop: 8347 - 14745, loop overhead 5947 - 11190 
		 *
		 *
		 *   Hyperthreaded core:
		 *           Pseudorandom access: 10748, loop overhead 9015
		 *           Sequential access: 12765, 
		 *           Sequential access in a loop: 12187, loop overhead: 17933
		 */

  		PR_OP1000(0);
		PR_OP1000(1);
		PR_OP1000(2);
		PR_OP1000(3);

		PR_OP10(400);
		PR_OP10(401);
		PR_OP10(402);
		PR_OP10(403);
		PR_OP10(404);
		PR_OP10(405);
		PR_OP10(406);
		PR_OP10(407);
		PR_OP10(408);
		
		OP(4090);
		OP(4094);		
		OP(4092);
		OP(4091);
		OP(4095);		
		OP(4093);

#endif

#if DEFINE_PSEUDO_2000
		
		/* Xeon 2048 operations (assumming 8KB cache, it has 128 lines each storing 16 4byte values) */
		/* Pseudorandom access */

  		PR_OP1000(0);
		PR_OP1000(1);
		
		PR_OP10(200);
		PR_OP10(201);
		PR_OP10(202);
		PR_OP10(203);
			
		OP(2045);
		OP(2040);
		OP(2042);
		OP(2047);
		OP(2041);
		OP(2044);		
		OP(2043);
		OP(2046);
		

#endif
		


		tl1 = RDTSCL();

		(*calldata)[i] = tl1 - tl0;
		(*returndata)[i] = local;

		//asm volatile ("mfence");

		*signal = 0;

		//asm volatile  ("sfence"); 

		std::cout << "Waiting for slave\n"; 
		
		do {

			asm volatile ("nop");

		//} while ( (*((int *)s_area + 1)) != 1 );		
		} while ( *signal != 1 );

	};

	
	/*
	 * Dump the data to console
	 */
	for ( int i = 0; i < DEPTH; i ++ ) {
                std::cout << "returndata:" << (*returndata)[i] << " calldata" << (*calldata)[i] << "\n";
        };


        return 0;
};

