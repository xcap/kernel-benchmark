#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sys/shm.h>

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}
inline volatile void NOP() {

        //asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
        asm volatile ("nop;");
        return;
}


inline volatile unsigned long int RDTSCL() {
     unsigned long int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}

inline volatile long long RDTSC1() {
   register long long TSC asm("eax");
   //asm volatile ("rdtsc" : : : "eax", "edx");
   //asm volatile (".byte 15, 49" : : : "eax", "edx");
   return TSC;
}

#define DEPTH 1000

#ifdef DEFINE_AMD_64
/* 64 bits for AMD 64 */
typedef unsigned long int test_t;
#endif

#ifdef DEFINE_AMD_32
/* 32 bits for AMD 64 */
typedef unsigned int test_t;
#endif

static test_t calldata [DEPTH + 1];
static test_t returndata [DEPTH + 1];

int shmid;
void *s_area = NULL;

#define S_SIZE 100*4096

int initialize_shm() {

	key_t key = 1116;

	shmid = shmget(key, S_SIZE, 0666);
	if (-1 == shmid) {

		if (ENOENT != errno)
			return -2;

		shmid = shmget(key, S_SIZE, IPC_CREAT | 0666);
		if (-1 == shmid) 
               		return -3;
	}

	s_area = shmat(shmid, NULL, 0);
	if (-1 == (long)s_area) {
		
		return -2;
	}

	return 0;	
};

test_t  * volatile var; 
int  * volatile signal;

#ifdef DEFINE_SNOOP_4000
#define SNOOP_DEPTH 4096
#endif 

#ifdef DEFINE_SNOOP_2000
#define SNOOP_DEPTH 2048
#endif

int main(void) {

        unsigned long int tl0;
        int ret;

        for ( int i = 0; i < DEPTH; i++) {
                calldata[i] = 0;
                returndata[i] = 0;
        };

	ret = initialize_shm();
	if (ret != 0) {
		std::cerr << "Unable to allocate shared memory region\n";
		return -1;
	};

	var = ((test_t *)s_area); 
	signal = (int *)((char*)s_area + S_SIZE - sizeof(int));	

	nice(-20);
	
	for ( int i = 0; i < DEPTH; i ++ ) {
		
		std::cout << "Waiting for master\n";
		
		do {
			asm volatile ("nop");

		} while ( *signal != 0 );

#define SHIFT 1

		/*
		 * Snoop variable 
		 */
		var = ((test_t *)s_area); 
		for( int j = 0; j < SNOOP_DEPTH; j ++) {  
			*var = i;
			var += SHIFT;
		};

		//asm volatile ("mfence"); /* to ensure that snooping completes before I fire up signal */

		*signal = 1; 

		//asm volatile ("sfence");	

	};
	
	/*
	 * Dump the data to console
	 */
//	for ( int i = 0; i < DEPTH; i ++ ) {
//                std::cout << calldata[i] << "\n";
//        };


        return 0;
};

