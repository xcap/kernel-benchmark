#!/bin/sh

CORE_MASTER=1
CORE_SLAVE_CMP=0
CORE_SLAVE_SMP=3
RES_DIR_SMP="results-smp-b"
RES_DIR_CMP="results-cmp-b"

RUNON_DIR="../mig-tools/runon"
EXP_CPU_NAME="pc-amd"

run_experiment ()
{
	g++ -O2 -o master master.cpp $DEFINES
	g++ -O2 -o slave slave.cpp $DEFINES
	sleep 3
	$RUNON_DIR $CORE_MASTER ./master > ${!RES_DIR}/"$EXP_CPU_NAME$EXP_NAME$EXP_LOOP_OVHD_NAME" &
	sleep 3
	$RUNON_DIR ${!CORE_SLAVE} ./slave
	wait
	sleep 3
}

mkdir $RES_DIR_SMP
mkdir $RES_DIR_CMP


# Define cores
CORES="SMP CMP"

for CORE in $CORES 
do

CORE_SLAVE=CORE_SLAVE_$CORE
RES_DIR=RES_DIR_$CORE

###################################################################
# 4096 operations migrating 32bit value
###################################################################

VAR_SIZES="32 64"

for VAR_SIZE in $VAR_SIZES 
do

SNOOP_DEPTHS="4000 2000"

for SNOOP_DEPTH in $SNOOP_DEPTHS
do
	
#
# Access in a loop
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_COST=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_ACCESS_IN_A_LOOP=1"
EXP_NAME="-${SNOOP_DEPTH}x$VAR_SIZE-seq-in-loop"
EXP_LOOP_OVHD_NAME=""
run_experiment

#
# Access in a loop overhead
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_OVHD=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_ACCESS_IN_A_LOOP=1"
EXP_LOOP_OVHD_NAME="-loop-overhead"
run_experiment

#
# Sequential access
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_COST=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_SEQ_$SNOOP_DEPTH=1"
EXP_NAME="-${SNOOP_DEPTH}x$VAR_SIZE-seq-access"
EXP_LOOP_OVHD_NAME=""
run_experiment

#
# Sequential access overhead
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_OVHD=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_SEQ_$SNOOP_DEPTH=1"
EXP_LOOP_OVHD_NAME="-loop-overhead"
run_experiment

#
# Pseudorandom access
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_COST=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_PSEUDO_$SNOOP_DEPTH=1"
EXP_NAME="-${SNOOP_DEPTH}x$VAR_SIZE-pseudo"
EXP_LOOP_OVHD_NAME=""
run_experiment

#
# Pseudorandom access overhead
#
DEFINES="-DDEFINE_AMD_$VAR_SIZE=y -DDEFINE_MEASURE_OVHD=y -DDEFINE_SNOOP_$SNOOP_DEPTH=y -DDEFINE_PSEUDO_$SNOOP_DEPTH=1"
EXP_LOOP_OVHD_NAME="-loop-overhead"
run_experiment

done #SNOOP_DEPTHS

done #VAR_SIZES

done

