#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sys/shm.h>


int main(void) {

        unsigned long int eax, ebx, edx, ecx;
        int ret;

	for (int i = 0; ; i++) {

		std::cout << "------------------------------------------------------\n";
				
		asm volatile  ("cpuid"
				: "=a" (eax), "=b" (ebx), "=c" (ecx)
				: "0" (4), "c" (i)
				: "dx");

	
		int cache_type = eax & 0xf;
		std::cout << "Cache type:";
		switch (cache_type) {
			case 0: 
				/* No more caches */
				return 1;
				std::cout << "no more caches.\n";
				break; 
			case 1:
				std::cout << "data cache\n";
				break;
	
        	        case 2:
                	        std::cout << "instruction cache\n";
                        	break;
	
        	        case 3:
                	        std::cout << "unified cache\n";
                        	break;		
		};

		int cache_level = (eax >> 5) & 7;
		std::cout << "Cache level:" << cache_level << "\n";

		int fully_associative = (eax >> 9) & 1;	
		std::cout << "Fully associative:";
		if ( fully_associative == 1 ) 
			std::cout << "yes\n";
		else 
			std::cout << "no\n";
	
		int line_size = (ebx & 0xfff) + 1; 
		std::cout << "Line size:" << line_size << "\n";

		int line_partitions = ((ebx >> 12 ) & 0x3ff) + 1; 
		std::cout << "Line partitions:" << line_partitions << "\n";

		int assoc_ways = ((ebx >> 22) & 0x3ff) + 1;
		std::cout << "Ways of associativity:" << assoc_ways << "\n";

		int set_number = ecx + 1; 
		std::cout << "Number of sets:" << set_number << "\n";

		std::cout << "Total cache size:" << line_size * line_partitions * assoc_ways * set_number << "\n" ; 

	};

	return 1; 

};
			
