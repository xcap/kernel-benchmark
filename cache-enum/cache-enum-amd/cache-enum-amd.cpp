#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sys/shm.h>


int main(void) {

        unsigned long int eax, ebx, edx, ecx;
        int ret;

	{
		std::cout << "------------------------------------------------------\n";
				
		asm volatile  ("cpuid"
				: "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
				: "0" (0x80000005), "c" (0)
				: );

	
		std::cout << "Cache type: data cache\n";

		int cache_level = 1;
		std::cout << "Cache level:" << cache_level << "\n";
	
		int line_size = (ecx & 0xff); 
		std::cout << "Line size:" << line_size << "\n";

		int assoc_ways = (ecx >> 16) & 0xff;
		std::cout << "Ways of associativity:" << assoc_ways << "\n";
	
		int lines_per_tag = (ecx >> 16) & 0xff; 
		std::cout << "Lines per tag:" << lines_per_tag << "\n";

		int cache_size = (ecx >> 24)  & 0xff;  
		std::cout << "Total cache size:" << cache_size << "KB\n" ;

		std::cout << "------------------------------------------------------\n";

		std::cout << "Cache type: instruction cache\n";

		cache_level = 1;
	        std::cout << "Cache level:" << cache_level << "\n";

	        line_size = (edx & 0xff);
	        std::cout << "Line size:" << line_size << "\n";

	        assoc_ways = (edx >> 16) & 0xff;
	        std::cout << "Ways of associativity:" << assoc_ways << "\n";

	        lines_per_tag = (edx >> 8) & 0xff;
	        std::cout << "Lines per tag:" << lines_per_tag << "\n";

	        cache_size = (edx >> 24)  & 0xff;
	        std::cout << "Total cache size:" << cache_size << "KB\n" ;

	};


	{
                std::cout << "------------------------------------------------------\n";

                asm volatile  ("cpuid"
                                : "=a" (eax), "=b" (ebx), "=c" (ecx), "=d" (edx)
                                : "0" (0x80000006), "c" (0)
                                : );


                std::cout << "Cache type: data cache\n";

                int cache_level = 2;
                std::cout << "Cache level:" << cache_level << "\n";

                int line_size = (ecx & 0xff);
                std::cout << "Line size:" << line_size << "\n";

                int assoc_ways = (ecx >> 12) & 0xf;
                std::cout << "Ways of associativity:";
		
		switch ( assoc_ways ) {

			case 0: std::cout << "cache is disabled\n";
				break; 
			case 1: std::cout << "direct mapped\n";
                                break;
			case 2: std::cout << "2\n";
                                break;
			case 4: std::cout << "4\n";
                                break;
			case 6: std::cout << "8\n";
                                break;
			case 8: std::cout << "16\n";
                                break;
			case 0xf: std::cout << "Fully associative\n";
                                break;		

		};

                int lines_per_tag = (ecx >> 8) & 0xf;
		std::cout << "Lines per tag:" << lines_per_tag << "\n";

                int cache_size = (ecx >> 16)  & 0xffff;
                std::cout << "Total cache size:" << cache_size << "KB\n" ;

                std::cout << "------------------------------------------------------\n";

	};

	
	

	return 1; 

};
			
