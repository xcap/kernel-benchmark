#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}
inline volatile void NOP() {

	//asm volatile ("nop; nop; nop; nop; nop; nop; nop; nop; nop; nop;");
	asm volatile ("nop;");
	return; 
}

inline volatile long long RDTSC1() {
   register long long TSC asm("eax");
   //asm volatile ("rdtsc" : : : "eax", "edx");
   //asm volatile (".byte 15, 49" : : : "eax", "edx");
   return TSC;
} 

#define rdtscl(low) \
	__asm__ __volatile__("lfence; rdtsc" : "=a" (low) : : "edx")

#define rdtsclx(low) \
	        __asm__ __volatile__("rdtsc" : : : )


inline volatile unsigned long int RDTSCL() {
     unsigned long int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}				

#define DEPTH 1000

static unsigned long int calldata [DEPTH + 1];
static unsigned long int returndata [DEPTH + 1];

unsigned long int factorial_o(unsigned long int i, unsigned long int &tl0);
unsigned long int factorial_e(unsigned long int i, unsigned long int &tl0);

unsigned long int dummy_inc(unsigned long int sum) {
	return sum + 1;
};

#if 0
unsigned long int factorial_o(unsigned long int i, unsigned long int &tl0) {

	unsigned long int tl1;
	unsigned long int sum = 0; 

	i -- ;
	if( i == 0 ) 
		return 0; 

	/* call cost */
	tl0 = RDTSCL();
	
	sum += factorial_e ( i , tl0);
	/* return cost */
	//tl1 = RDTSCL();

	//returndata[DEPTH - i] = tl1 - tl0;

	sum++; 
	/* return cost */
	//tl0 = RDTSCL();
	return sum; 
};

unsigned long int factorial_e(unsigned long int i, unsigned long int &tl0) {

	unsigned long int tl1;

	/* call cost */
	tl1 = RDTSCL();

	//std::cout << "DEPTH - i" << DEPTH - i << "\n";
	calldata [DEPTH - i] = tl1 - tl0;

	unsigned long int sum = 0; 

	i -- ;
	if( i == 0 ) 
		return 0; 

	sum += factorial_o ( i , tl0);

	/* return cost */
	//tl1 = RDTSCL();

	//returndata[DEPTH - i] = tl1 - tl0;

	sum++; 
	/* return cost */
	//tl0 = RDTSCL();
	return sum; 
};
#endif

unsigned long int factorial(unsigned long int i, unsigned long int &tl0) {

	unsigned long int tl1;

	/* call cost */
	tl1 = RDTSCL();

	//std::cout << "DEPTH - i" << DEPTH - i << "\n";
	calldata [DEPTH - i] = tl1 - tl0;

	unsigned long int sum = 0; 

	i -- ;
	if( i == 0 ) 
		return 0; 

	/* call cost */
	tl0 = RDTSCL();
	sum += dummy_inc (sum); 
	sum += dummy_inc (sum); 
	sum += dummy_inc (sum);
	sum += dummy_inc (sum);
	sum += dummy_inc (sum);
	sum += dummy_inc (sum);
	sum += dummy_inc (sum);


	sum += factorial ( i , tl0);
	/* return cost */
	tl1 = RDTSCL();

	returndata[DEPTH - i] = tl1 - tl0;

	sum++; 
	/* return cost */
	tl0 = RDTSCL();
	return sum; 
};

unsigned long int flat_invocation() {

	unsigned long int sum = 0; 
	unsigned long int tl0, tl1; 

	for (int i = 0; i < DEPTH; i ++ ) {
		tl0 = RDTSCL();

		/* 206, 209 for data in A series below 207 was used */

		/*
		 * Interesting stuff: loop of nops costs as below: 
		 * i = 9 -> 97 105 98 
		 * i = 10 -> 105 113 113 113 112 112 112 112 113 113 113 113 112 112 112 
		 * however if we add dummy_inc the cost drops back to the 97 98 105, until
		 * we add third dummy_inc which increases cost to 128 ....
		 */
		/*
		 * i = 100 -> 308
		 * i = 200 -> 510
		 * i = 300 -> 712 
		 * i = 400 -> 908
		 * i = 500 -> 1110
		 * i = 600 -> 1312
		 * 
		 * Conclusion 100 nop instruction costs 200 cycles in the following loop 
		 * 
		 * movl   $599, %eax
		 * 
		 * L19: nop
		 *      decl    %eax
		 *      jne     L19
		 */ 
		for (int j = 1; j < 600; j ++ ) {
			asm volatile ("nop");
		};

#if 0
		/* A: 525 -> 533 */
		/* 97 98 105 */
		sum += dummy_inc (sum); 

		/* A: 525 -> 548 
		 * 97 98 105 */
		sum += dummy_inc (sum); 
//#if 0
		/* A: 525 -> 555 */
		/* 128 128 128 128 120 120 120 120 127 127 127 127 120 120 120 */
		sum += dummy_inc (sum);
		/* A: 570 */
		/* 135 135 135 135 135 135 135 135 135 127 128 127 128 127 128 */
		sum += dummy_inc (sum);
//#if 0		/* A: 585 - 578 */
		/* the whole serie executed in rounds 135 143 142 ... */
		sum += dummy_inc (sum);
		
//#if 0		/* A: 593 */
		sum += dummy_inc (sum);
		/* A: 600 - 608 */
		sum += dummy_inc (sum);
		//sum += dummy_inc (sum);

#endif
		tl1 = RDTSCL();

		calldata[i] = tl1 - tl0;
	};

	return sum; 
};

static void dummy_foo() {
	return; 
}

unsigned long int flat_invocation_2() {

	unsigned long int sum = 0; 
	unsigned long int tl0, tl1; 

	for (int i = 0; i < DEPTH; i ++ ) {
		tl0 = RDTSCL();

		/* 206, 209 for data in A series below 207 was used */

		/*
		 * Interesting stuff: loop of nops costs as below: 
		 * i = 9 -> 97 105 98 
		 * i = 10 -> 105 113 113 113 112 112 112 112 113 113 113 113 112 112 112 
		 * however if we add dummy_inc the cost drops back to the 97 98 105, until
		 * we add third dummy_inc which increases cost to 128 ....
		 */
		/*
		 * i = 100 -> 308
		 * i = 200 -> 510
		 * i = 300 -> 712 
		 * i = 400 -> 908
		 * i = 500 -> 1110
		 * i = 600 -> 1312
		 * 
		 * Conclusion 100 nop instruction costs 200 cycles in the following loop 
		 * 
		 * movl   $599, %eax
		 * 
		 * L19: nop
		 *      decl    %eax
		 *      jne     L19
		 */ 
		for (int j = 1; j < 100; j ++ ) {
			asm volatile ("nop");
		};


		dummy_foo ();
	       	dummy_foo ();
		dummy_foo ();
		dummy_foo ();
		dummy_foo ();
		dummy_foo (); 
		
		tl1 = RDTSCL();

		calldata[i] = tl1 - tl0;
	};

	return sum; 
};

							       
int main(void) {

	unsigned long int tl0;
	unsigned long int sum; 

	for ( int i = 0; i < DEPTH; i++) {
		calldata[i] = 0;
		returndata[i] = 0;
	};

	tl0 = RDTSCL();

	sum = flat_invocation_2();
	
	//sum = flat_invocation();
	
	//sum = factorial ( DEPTH, tl0); 

	//sum = factorial_e ( DEPTH, tl0);
	
	for ( int i = 0; i < DEPTH; i ++ ) {
		//std::cout << "call cost:" << calldata[i] << " return cost:"<< returndata[i] <<"\n";
		std::cout << calldata[i] << "\n";
	};

	std::cout << "Sum is: " << sum << "\n";

	return 0;
};
