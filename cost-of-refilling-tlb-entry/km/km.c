
#include "km.h"


inline volatile unsigned long int RDTSCL() {
     unsigned long int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}


#define local_irq_disable() 	__asm__ __volatile__("cli": : :"memory")
#define local_irq_enable()	__asm__ __volatile__("sti": : :"memory")

#define INVL_OP(x)	__asm__ __volatile__("invlpg %0": :"m" (*( ((char *)p) + (x)*SHIFT ) ) )

#define INVL_OP10(_x) INVL_OP( (_x *10 + 0) ); INVL_OP( (_x *10 + 1) ); INVL_OP( (_x *10 + 2) ); INVL_OP( (_x *10 + 3) ); \
                 INVL_OP( (_x *10 + 4) ); INVL_OP( (_x *10 + 5) ); INVL_OP( (_x *10 + 6) ); INVL_OP( (_x *10 + 7) ); \
                 INVL_OP( (_x *10 + 8) ); INVL_OP( (_x *10 + 9) );


#define SHIFT 	PAGE_SIZE

#define OP(x)	__asm__ __volatile__ ("movl (%0), %%eax\n" : :"r"(((char*)p) + (x)*SHIFT): "eax")

#define OP10(_x) OP( (_x *10 + 0) ); OP( (_x *10 + 1) ); OP( (_x *10 + 2) ); OP( (_x *10 + 3) ); \
                 OP( (_x *10 + 4) ); OP( (_x *10 + 5) ); OP( (_x *10 + 6) ); OP( (_x *10 + 7) ); \
                 OP( (_x *10 + 8) ); OP( (_x *10 + 9) );

#define OP100(_x) OP10( (_x *10 + 0) ); OP10( (_x *10 + 1) ); OP10( (_x *10 + 2) ); OP10( (_x *10 + 3) ); \
                  OP10( (_x *10 + 4) ); OP10( (_x *10 + 5) ); OP10( (_x *10 + 6) ); OP10( (_x *10 + 7) ); \
                  OP10( (_x *10 + 8) ); OP10( (_x *10 + 9) );

#define OP1000(_x) OP100( (_x *10 + 0) ); OP100( (_x *10 + 1) ); OP100( (_x *10 + 2) ); OP100( (_x *10 + 3) ); \
                   OP100( (_x *10 + 4) ); OP100( (_x *10 + 5) ); OP100( (_x *10 + 6) ); OP100( (_x *10 + 7) ); \
                   OP100( (_x *10 + 8) ); OP100( (_x *10 + 9) );

#define DEPTH 100

/*
 * Xeon has 64 data TLB entries
 */
#define DATA_TLB_ENTRY 64

unsigned long int (*calldata) [DEPTH + 1];
unsigned long int (*calldata2) [DEPTH + 1];
unsigned long int (*returndata) [DEPTH + 1];
unsigned long int *mem_area; 
unsigned long int *p; 
unsigned long int tl0, tl1;


static int tt_main(void *arg)
{
	int i, j;

	calldata = NULL;
	calldata2 = NULL;
	returndata = NULL; 
	mem_area = NULL;

	TT_DBG("inside tt_main!\n");

	/* Allocate memory */
	
	calldata = (long unsigned int (*)[DEPTH + 1])  kmalloc(sizeof(unsigned long int) * (DEPTH + 1), GFP_KERNEL);
	if (!calldata ) {
		TT_ERR("Unable to allocate calldata\n");
		return 0;
	};

	calldata2 = (long unsigned int (*)[DEPTH + 1])  kmalloc(sizeof(unsigned long int) * (DEPTH + 1), GFP_KERNEL);
	if (!calldata2 ) {
		TT_ERR("Unable to allocate calldata\n");
		return 0;
	};
		
	mem_area = (long unsigned int *) vmalloc(PAGE_SIZE * (DATA_TLB_ENTRY + 1));
	if (!mem_area ) {
		TT_ERR("Unable to allocate mem_area\n");
		return 0;
	};

	/*
	 * IMPORTANT!!! without next line OP macroses will crash!!!
	 */
	p = mem_area;
        
	/*
	 * Initialize calldata
	 */
        for ( i = 0; i < DEPTH; i++) {
                (*calldata)[i] = 0;
		(*calldata2)[i] = 0;
                //(*returndata)[i] = 0;
        };

	/*
	 * Cost of refilling TLB Entry (Xeon):
	 *
	 * cost of invalidating 64 TLB entries: 33255
	 * cost of refilling 64 TLB entries: 1335
	 * 
	 * mfences are everywhere:
	 * cost of invalidating 64 TLB: 33143
	 * cost of refilling 64 TLB: 1297
	 * 
	 * Overhead: 
	 * 	cost of two back to back rdtsc invocations: 172, 
	 * 	cost of accessing 64 memory entries: 277
	 *	
	 *	cost of accessing 64 memory locations (suppose L1 cache): 330 - 435
	 *	(mfence is before and after)	
	 * 
	 * The same experiment with loops:
	 * cost of invalidating 64 TLB entries: 32940, 
	 * cost of refilling 64 TLB entries: 32933
	 *
	 *
	 */
	for ( i = 0; i < DEPTH; i++) {
		
		/*
		 * Ensure pages are mapped to memory and data is in cache
		 */

		
		//tl0 = RDTSCL();

		__asm__ __volatile__ ("mfence");
#if 1		
		OP10(0);
		OP10(1);
		OP10(2);
		OP10(3);
		OP10(4);
		OP10(5);

		OP(60);
		OP(61);
		OP(62);
		OP(63);
		OP(64);
#endif			
		__asm__ __volatile__ ("mfence");
		
		//tl1 = RDTSCL();

        	//(*calldata)[i] = tl1 - tl0;
		
		/*
		 * Invalidate 64 Xeon data TLB entries
		 */
#if 1		
		tl0 = RDTSCL();

#if 0
		/*
		 * Invalidate TLB entries one by one
		 */
		INVL_OP10(0);
		INVL_OP10(1);
		INVL_OP10(2);
		INVL_OP10(3);
		INVL_OP10(4);
		INVL_OP10(5);

		INVL_OP(60);
		INVL_OP(61);
		INVL_OP(62);
		INVL_OP(63);
		INVL_OP(64);

#endif

#if 0
		/*
		 * Invalidate all TLB entries 
		 */
		__flush_tlb();
#endif

#if 1
                /*
		 * Invalidate all TLB globally (see PGE flag)
		 */
                __flush_tlb_global();
#endif
				
		
#if 0		
		for ( j = 0; j < DATA_TLB_ENTRY; j ++ ) {
			INVL_OP(j);
		};
#endif
		tl1 = RDTSCL();

        	(*calldata2)[i] = tl1 - tl0;
#endif			


		/*
		 * Try to refill TLB entries
		 */

		__asm__ __volatile__ ("mfence");

		tl0 = RDTSCL();
#if 1
		OP10(0);
		OP10(1);
		OP10(2);
		OP10(3);
		OP10(4);
		OP10(5);

		OP(60);
		OP(61);
		OP(62);
		OP(63);
		OP(64);
#endif
		
#if 0		
		for ( j = 0; j < DATA_TLB_ENTRY; j ++ ) {
			INVL_OP(j);
		};
#endif
		
		tl1 = RDTSCL();

		__asm__ __volatile__ ("mfence");

        	(*calldata)[i] = tl1 - tl0;
	
	};

	/*
         * Dump the data to console
         */
        for ( i = 0; i < DEPTH; i ++ ) {
                TT_DBG("cost of invalidating TLB: %d, cost of refilling TLB: %d\n", (*calldata2)[i], (*calldata)[i]);
        };


	return 0;
}

//static tt_ops_t	tt_def_ops = {
//	.loginit = tt_loginit,
//	.lognet	 = tt_log_dummy_pkt
//};

static int __init tt_init(void)
{

	/*
	 * kick off the main TT thread.
	 */
	tt_task = kthread_run(tt_main, NULL, "tt_main");
	if (IS_ERR(tt_task)) {
		/*
		 * better err val?
		 */
		return -ENODEV;
	}
	TT_DBG("inside tt_init!\n");
	return 0;
}

void __exit tt_exit(void)
{
	
	TT_DBG("Unloading the module :-(...\n");

	if( calldata )
		kfree(calldata);
	
	if( calldata2 )
		kfree(calldata2);

	if( mem_area )
		vfree(mem_area);

	return;
}

module_init(tt_init);
module_exit(tt_exit);
MODULE_LICENSE("GPL");
