/*	
	File Name:	$Source: /usr/cvs/os/migrate_one.cpp,v $
	CVS Version:	$Revision: 1.1.1.1 $
	Last Modified:	$Author: root $
	Date Modified:	$Date: 2003/05/31 16:12:42 $
	
	Original Author:	Dave Nellans <dnellans@cs.utah.edu>
       	Copyright:	(C) 2003  Dave Nellans and the University of Utah

	Description:	Simple utility to set and reset the cpu_allowed masks for a process, effectively migrating or locking down
			a process onto one cpu.
	
	To Do:		Complete Implementation.	*/

#include <stdlib.h>
#include <sched.h>
#include <iostream>
using namespace std;

int main(int argc, char * argv[])
{	
	unsigned long new_mask;
	unsigned long cur_mask;
	unsigned int len = sizeof(new_mask);
	pid_t pid;

	/* Check for right argument number */
	if (argc != 3) {
		cout << "Usage: " << argv[0] << " <pid> <cpu_mask>" << endl;
		return -1;
	}

	/* Convert pid from char string to pid */
	pid = atol(argv[1]);
	
	/* Convert second arg from char string to unsigned long reading up to maximum field width of 8 */
	sscanf(argv[2], "%08lx", &new_mask);

	/* Gets old affinity and prints it out, or error if can not get it*/
	if (sched_getaffinity(pid, len, (cpu_set_t* ) &cur_mask) < 0) {
		cout << "Error: Couldn't get pid " << pid << "'s affinity." << endl;
		return -1;
	}

	cout << "pid " << pid << "'s old affinity: " << cur_mask << endl;

	/* Sets affinity and prints error if it could not set it */
	if (sched_setaffinity(pid, len, (cpu_set_t* ) &new_mask)) {
		
		cout << "Error: Couldn't set pid " << pid << "'s affinity." << endl;
		return -1;
	}

	/* Gets affinity to check it was set and prints it out, error if can not */
	if (sched_getaffinity(pid, len, (cpu_set_t* ) &cur_mask) < 0) {
		
		cout << "Error: Couldn't get pid " << pid << "'s affinity" << endl;
		return -1;
	}

	cout << "pid " << pid << "'s new affinity: " << cur_mask << endl;
	
	return 0;
}
