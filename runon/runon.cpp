/*	
	File Name:	$Source: /usr/cvs/os/migrate_one.cpp,v $
	CVS Version:	$Revision: 1.1.1.1 $
	Last Modified:	$Author: root $
	Date Modified:	$Date: 2003/05/31 16:12:42 $
	
	Original Author:	Dave Nellans <dnellans@cs.utah.edu>
       	Copyright:	(C) 2003  Dave Nellans and the University of Utah

	Description:	Simple utility to set and reset the cpu_allowed masks for a process, effectively migrating or locking down
			a process onto one cpu.
	
	To Do:		Complete Implementation.	*/

#include <stdlib.h>
#include <sched.h>
#include <iostream>
#include <unistd.h>

using namespace std;

int main(int argc, char * argv[])
{	
  cpu_set_t new_mask;
  cpu_set_t cur_mask;
  unsigned long bit_mask;
  unsigned int int_mask;

  /* Check for right argument number */
  if (argc < 3)
    {
      cout <<  "Usage: " << argv[0] << " <cpu_mask> <program> [program args]\n";
      return -1;
    }

  /* Convert second arg from char string to unsigned long reading up to maximum field width of 8 */
  //sscanf(argv[1], "%08lx", (unsigned long*)&bit_mask);
  sscanf(argv[1], "%i", &int_mask); 
  CPU_ZERO(&new_mask);
  CPU_SET(int_mask, &new_mask);

  /* Sets affinity and prints error if it could not set it */
  int ret; 
  ret = sched_setaffinity(0, sizeof(new_mask), (cpu_set_t*) &new_mask);
 if (ret)
    {
	  
      cout << "Error: Couldn't set " << argv[1] << "'s affinity. Error:"<< ret <<"\n";
      return -1;
    }

  /* Gets affinity to check it was set and prints it out, error if can not */
  if (sched_getaffinity(0, sizeof(cur_mask), (cpu_set_t*) &cur_mask) < 0)
    {
      cout << "Error: Couldn't get " << argv[2] << "'s new affinity\n";
      return -1;
    }

  cout << "Running " << argv[2] << " at new affinity: " << *(unsigned long *)&cur_mask << endl;

  execvp(argv[2], &argv[2]);
	
  return 0;
}
