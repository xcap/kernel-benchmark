#include <pthread.h>
#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>
#include <sched.h>



inline volatile unsigned long int RDTSCL() {
     unsigned int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}

#define __syscall_return(type, res) \
	return (type) (res); 

#define __NR_km_launch		312
#define __NR_km_yield		313


/* XXX - _foo needs to be __foo, while __NR_bar could be _NR_bar. */

#define _syscall0(type,name) 				\
inline volatile type name(void) 			\
{ 							\
	unsigned long int __res; 			\
	__asm__ volatile ("int $0x80" 			\
			: "=a" (__res) 			\
			: "0" (__NR_##name)); 		\
			return __res; 			\
}

#define _syscall1(type,name,type1,arg1) 		\
type name(type1 arg1) 					\
{ 							\
long __res; 						\
__asm__ volatile ("int $0x80" 				\
	: "=a" (__res) 					\
	: "0" (__NR_##name),"b" ((long)(arg1)) : "memory"); \
	return __res; 					\
}

//_syscall0(int, km_launch);
//_syscall0(int, km_yield);

_syscall1(int, km_launch, int, flag);
_syscall1(int, km_yield, int, flag);

#define DEPTH 5000


int shmid;
void *s_area = NULL;

/*
 * We will use shared memory for sharing results
 */
#define S_SIZE (2*2*sizeof(long int)*DEPTH)

int initialize_shm() {

        key_t key = 1113;

        shmid = shmget(key, S_SIZE, 0666);

        if (-1 == shmid) {


           if (ENOENT != errno) {
               return -2;
           }

           shmid = shmget(key, S_SIZE, IPC_CREAT | 0666);

           if (-1 == shmid) {

               return -3;

           }


       }

        s_area = shmat(shmid, NULL, 0);
        if (-1 == (long)s_area) {

                return -4;
        }

        return 0;

};

unsigned long int local_calldata [DEPTH];
unsigned long int (*calldata_launch) [DEPTH];
unsigned long int (*calldata_yield) [DEPTH];

int launched = 0; 

pthread_t thread;
pthread_mutex_t mut = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t cond = PTHREAD_COND_INITIALIZER;

void thread_main(void *ptr) {
	int i; 

	pthread_mutex_lock(&mut);
        
	while ( launched == 0) {
		
		pthread_cond_wait(&cond, &mut);
	}
        
	pthread_mutex_unlock(&mut);

	sched_yield();
	
	for ( i = 0; i < DEPTH; i ++ ) {

		pthread_mutex_lock(&mut);

		launched = i;

		pthread_mutex_unlock(&mut);		

                (*calldata_yield)[i] = RDTSCL();
                //printf("yield-user: entering %i\n", i);
                sched_yield();
                //printf("yield-user: exiting %i\n", i);
        };
	
	printf("Yield: returned\n");

	return ; 

};

int main(void){

	int ret;
	int i; 
	unsigned long int t1, t0; 

	ret = initialize_shm();
        if (ret != 0) {
                printf("Unable to allocate shared memory region %i\n", ret);
                return -1;
        };
	
		
	calldata_launch = (long unsigned int (*)[DEPTH])s_area;
        calldata_yield = (long unsigned int (*)[DEPTH])((char *)(s_area) + (S_SIZE)/2);

	nice(-10);

	pthread_create( &thread, NULL, (void * (*)(void *)) &thread_main, ( void *) NULL);


	/*
	 * Cost of thread switch: 2182, 
	 * cost of local yield (2 context switches): 4485
	 * cost of yield in the kernel: 3533
	 *
	 */

	pthread_mutex_lock(&mut);
        
	launched = 1;
		
	pthread_cond_broadcast(&cond);	
        
	pthread_mutex_unlock(&mut);

	sched_yield();
	
	for ( i = 0; i < DEPTH - 1;  ) {
		//printf("launch-user: entering %i\n", i);
		//
		t0 = RDTSCL();
		sched_yield();
		t1 = RDTSCL();
		
		pthread_mutex_lock(&mut);
		
		i = launched;
		(*calldata_launch)[i] = t1;
		local_calldata[i] = t1 - t0; 	

		

		pthread_mutex_unlock(&mut);		
		//printf("launch-user: exiting %i\n", i);

	};


	pthread_join(thread, NULL);
	
	printf("Launch: returned\n");

	for ( i = 0; i < DEPTH; i ++ ) {
                printf("Process switch cost: %d, local yield cost: %d\n", (*calldata_launch)[i] - (*calldata_yield)[i], local_calldata[i]);
        };

	shmdt(s_area);
	
	return 0;
}

