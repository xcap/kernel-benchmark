#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>

inline volatile unsigned long int RDTSCL() {
     unsigned int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}

#define __syscall_return(type, res) \
	return (type) (res); 

#define __NR_costkernel         311

/* XXX - _foo needs to be __foo, while __NR_bar could be _NR_bar. */

#if 1
#define _syscall0(type,name) \
inline volatile type name(void) \
{ \
unsigned long int __res; \
__asm__ volatile ("int $0x80" \
	: "=a" (__res) \
	: "0" (__NR_##name)); \
	return __res; \
}

#endif

#if 0
/* XXX - _foo needs to be __foo, while __NR_bar could be _NR_bar. */
#define _syscall0(type,name) \
inline volatile type name(void) \
{ \
unsigned long int __res; \
__asm__ volatile ("pushl %%edx; int $0x82; popl %%edx" \
	: "=a" (__res) \
	: "0" (__NR_##name)); \
	return __res; \
}
#endif

_syscall0(int, costkernel);

#define DEPTH 1000

unsigned long int (*local_calldata) [DEPTH];

int main(void){

	int res;
	int i;

	long long t0, t1;
	unsigned long int tl0, tl1; 

	local_calldata = (long unsigned int (*)[DEPTH]) malloc (sizeof(unsigned long int)*DEPTH);

	nice(-5);

	for( i = 0; i < DEPTH ; i ++) {
		tl0 = RDTSCL(); 

#if 1
		/*
		 * Via sysenter
		 */
		syscall(311);
#endif

#if 0 		
		/*
		 * Via int 0x80
		 */
		costkernel();
#endif		

		tl1 = RDTSCL();
	
		(*local_calldata)[i] = tl1 - tl0;	

	};

	for ( i = 0; i < DEPTH; i ++ ) {

                printf("Time to do a system call: %u\n", (*local_calldata)[i]);
        };



	free (local_calldata);

	return 0;
}

