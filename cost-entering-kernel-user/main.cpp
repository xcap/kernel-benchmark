#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>


inline volatile unsigned long int RDTSCL() {
     unsigned int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}

inline volatile long long RDTSC() {
     unsigned long long int x;
     //__asm__ volatile (".byte 0x0f, 0x31" : "=A" (x));
     __asm__ __volatile__ ("rdtsc" : "=A" (x));
     return x;
}

#define __syscall_return(type, res) \
	return (type) (res); 

#define __NR_costkernel         311

/* XXX - _foo needs to be __foo, while __NR_bar could be _NR_bar. */
#define _syscall0(type,name) \
inline volatile type name(void) \
{ \
unsigned long int __res; \
__asm__ volatile ("int $0x82" \
	: "=a" (__res) \
	: "0" (__NR_##name)); \
	return __res; \
}

#if 0
/* XXX - _foo needs to be __foo, while __NR_bar could be _NR_bar. */
#define _syscall0(type,name) \
inline volatile type name(void) \
{ \
unsigned long int __res; \
__asm__ volatile ("pushl %%edx; int $0x82; popl %%edx" \
	: "=a" (__res) \
	: "0" (__NR_##name)); \
	return __res; \
}
#endif

_syscall0(int, costkernel);

int main(void){

	int res;

	long long t0, t1;
	unsigned long int tl0, tl1; 

	for(int i = 0; i < 10000 ; i ++) {
		tl0 = RDTSCL(); 
		//t0 = RDTSC();
		//tl1 = syscall(311);
		
//        int a=10, b;

		tl1 = costkernel();

	//        asm ("movl %%edx, %0;"
	 //            :"=r"(t1)        /* output */
       // 	     : :
        //	     );  
	
	//	 costkernel();
		
//		t1 = RDTSC();

		//tl1 = RDTSCL();
		printf("\ntime to do a system call: %u", tl1 - tl0);
	//	printf("\ntl1: %i", tl1);
		//printf("\ntime to do a system call: %llu", t1 - t0);

	};

	return 0;
}

