#include <syscall.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <errno.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string>

#define __NR_km_send_ipi	311

#define _syscall0(type,name)                            \
inline volatile type name(void)                         \
{                                                       \
        unsigned long int __res;                        \
        __asm__ volatile ("int $0x80"                   \
                        : "=a" (__res)                  \
                        : "0" (__NR_##name));           \
                        return __res;                   \
}

#define _syscall1(type,name,type1,arg1)                 \
type name(type1 arg1)                                   \
{                                                       \
long __res;                                             \
__asm__ volatile ("int $0x80"                           \
        : "=a" (__res)                                  \
        : "0" (__NR_##name),"b" ((long)(arg1)) : "memory"); \
        return __res;                                   \
}

_syscall1(int, km_send_ipi, int, flag);

#define DEPTH 100

int main(void){
	int i; 

	/*
	 * Cost of sending IPI to the second CPU (SMP CPU) is: 
	 * rtt:29520, send time:375, receiver body:98 
	 * to hyperthreading core: rtt:4770, sender:428, receiver:98
	 * to itself: rtt:6892, sender:375, receiver:98
	 *
	 */
	
	for (int i=0; i < DEPTH; i++) {
		//sleep();
		km_send_ipi(2);
	};

	/* dump data */
	km_send_ipi(128);
	return 0; 
};

