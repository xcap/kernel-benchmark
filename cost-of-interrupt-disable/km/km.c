
#include "km.h"

static struct task_struct       *tt_task;

inline volatile unsigned long int RDTSCL() {
     unsigned long int x;
     __asm__ __volatile__("rdtsc" : "=a" (x) : : "edx");
     return x;
}


#define local_irq_disable() 	__asm__ __volatile__("cli": : :"memory")
#define local_irq_enable()	__asm__ __volatile__("sti": : :"memory")

#define OP(x)    local_irq_disable(); \
		 local_irq_enable()

#define OPD(x)    local_irq_disable()
#define OPE(x)    local_irq_enable()


#define OP10(_x) OP( (_x *10 + 0) ); OP( (_x *10 + 1) ); OP( (_x *10 + 2) ); OP( (_x *10 + 3) ); \
                 OP( (_x *10 + 4) ); OP( (_x *10 + 5) ); OP( (_x *10 + 6) ); OP( (_x *10 + 7) ); \
                 OP( (_x *10 + 8) ); OP( (_x *10 + 9) );

#define OP100(_x) OP10( (_x *10 + 0) ); OP10( (_x *10 + 1) ); OP10( (_x *10 + 2) ); OP10( (_x *10 + 3) ); \
                  OP10( (_x *10 + 4) ); OP10( (_x *10 + 5) ); OP10( (_x *10 + 6) ); OP10( (_x *10 + 7) ); \
                  OP10( (_x *10 + 8) ); OP10( (_x *10 + 9) );

#define OP1000(_x) OP100( (_x *10 + 0) ); OP100( (_x *10 + 1) ); OP100( (_x *10 + 2) ); OP100( (_x *10 + 3) ); \
                   OP100( (_x *10 + 4) ); OP100( (_x *10 + 5) ); OP100( (_x *10 + 6) ); OP100( (_x *10 + 7) ); \
                   OP100( (_x *10 + 8) ); OP100( (_x *10 + 9) );

#define OPE10(_x) OPE( (_x *10 + 0) ); OPE( (_x *10 + 1) ); OPE( (_x *10 + 2) ); OPE( (_x *10 + 3) ); \
                  OPE( (_x *10 + 4) ); OPE( (_x *10 + 5) ); OPE( (_x *10 + 6) ); OPE( (_x *10 + 7) ); \
                  OPE( (_x *10 + 8) ); OPE( (_x *10 + 9) );

#define OPE100(_x) OPE10( (_x *10 + 0) ); OPE10( (_x *10 + 1) ); OPE10( (_x *10 + 2) ); OPE10( (_x *10 + 3) ); \
	          OPE10( (_x *10 + 4) ); OPE10( (_x *10 + 5) ); OPE10( (_x *10 + 6) ); OPE10( (_x *10 + 7) ); \
                  OPE10( (_x *10 + 8) ); OPE10( (_x *10 + 9) );


#define OPD10(_x) OPD( (_x *10 + 0) ); OPD( (_x *10 + 1) ); OPD( (_x *10 + 2) ); OPD( (_x *10 + 3) ); \
	         OPD( (_x *10 + 4) ); OPD( (_x *10 + 5) ); OPD( (_x *10 + 6) ); OPD( (_x *10 + 7) ); \
                 OPD( (_x *10 + 8) ); OPD( (_x *10 + 9) );

#define OPD100(_x) OPD10( (_x *10 + 0) ); OPD10( (_x *10 + 1) ); OPD10( (_x *10 + 2) ); OPD10( (_x *10 + 3) ); \
	          OPD10( (_x *10 + 4) ); OPD10( (_x *10 + 5) ); OPD10( (_x *10 + 6) ); OPD10( (_x *10 + 7) ); \
                  OPD10( (_x *10 + 8) ); OPD10( (_x *10 + 9) );


#define DEPTH 100

unsigned long int (*calldata) [DEPTH + 1];
unsigned long int (*returndata) [DEPTH + 1];
unsigned long int tl0, tl1;


static int tt_main(void *arg)
{
	int i; 

	TT_DBG("inside tt_main!\n");

	/* Allocate memory */
	
	calldata = (long unsigned int (*)[DEPTH + 1])  kmalloc(sizeof(unsigned long long int) * (DEPTH + 1), GFP_KERNEL);
        
	//returndata = (long unsigned int (*)[DEPTH + 1])  kmalloc(sizeof(unsigned long long int) * (DEPTH + 1), GFP_KERNEL);	

        for ( i = 0; i < DEPTH; i++) {
                (*calldata)[i] = 0;
                //(*returndata)[i] = 0;
        };

	/*
	 * Cost of disabling and enabling interrupts (Xeon):
	 * 	100 int_disable, int_enable: 11700
	 *
	 * 	One int enable/disable cycle: 117
	 *
	 */
	for ( i = 0; i < DEPTH; i++) {
		tl0 = RDTSCL();

#if 1		
		OP100(0);
#endif

#if 0
                OPD100(0);                
                OPE(0);
#endif
		
#if 0
		OPD(0);
		OPE100(0);
#endif 		
		tl1 = RDTSCL();

        	(*calldata)[i] = tl1 - tl0;
	
	};

	/*
         * Dump the data to console
         */
        for ( i = 0; i < DEPTH; i ++ ) {
                TT_DBG("calldata: %d\n", (*calldata)[i]);
        };


	return 0;
}

//static tt_ops_t	tt_def_ops = {
//	.loginit = tt_loginit,
//	.lognet	 = tt_log_dummy_pkt
//};

static int __init tt_init(void)
{

	/*
	 * kick off the main TT thread.
	 */
	tt_task = kthread_run(tt_main, NULL, "tt_main");
	if (IS_ERR(tt_task)) {
		/*
		 * better err val?
		 */
		return -ENODEV;
	}
	TT_DBG("inside tt_init!\n");
	return 0;
}

void __exit tt_exit(void)
{
	
	TT_DBG("Unloading the module :-(...\n");

	if( calldata )
		kfree(calldata);

	return;
}

module_init(tt_init);
module_exit(tt_exit);
MODULE_LICENSE("GPL");
