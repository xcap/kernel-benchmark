#include <linux/config.h>
#include <linux/version.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/uio.h>
#include <asm/string.h>
#include <asm/io.h>
#include <asm/setup.h>
#include <asm/uaccess.h>

static int errno;
#include <linux/ip.h>
#include <linux/in.h>
#include <linux/netdevice.h>
#include <linux/etherdevice.h>

#include <asm/pgalloc.h>
#include <linux/kernel.h>
#include <linux/vmalloc.h>
#include <linux/blkdev.h>
#include <linux/kthread.h>
#include <linux/module.h>
#include <linux/interrupt.h>


#define TT_DEBUG

#ifdef TT_DEBUG
#define TT_DBG(_f, _a...)	printk("TT_LOG:(%s) " _f, __FUNCTION__, ## _a)
#else /* TT_DEBUG */
#define TT_DBG(a)		do { } while(0)
#endif /* TT_DEBUG */

#define TT_ERR(_f, _a...)	printk("TT_LOG:(%s) ERROR " _f, \
						__FUNCTION__, ## _a)


